import jwt from "jsonwebtoken";

import { getMessage, report } from "../data-codes.js";

const verify = (request , response, next) => {
  // Capturamos el token de autenticacion
  const token = request.header('x-auth-token');

  if (!token) {
    return response.status(401).send({
      ...getMessage('MA0003')
    })
  }

  try {
    const cifrado = jwt.verify(token, process.env.SECRET_WORD);

    request.credentials = cifrado.data;

    next();

  } catch (error) {
    const code = 'MA0000';
    report(code);
    console.log(error);
    return response.status(401).send(getMessage(code));
  }
}

export default verify;