
import chalk from "chalk";

const messages = [
  {
    code: '0000',
    msg: 'Codigo de error no registrado'
  },
  {
    code: '0001',
    msg: 'Error al intentar conectar con la Database'
  },
  {
    code: '0002',
    msg: 'Error al crear el token'
  },
  /*
  ==================================================
            Nomenclatura
            [A-Z] { 2 } : Abreviatura del modelo o coleccion
            [0-9] { 1 } : Status
              0 : error
              1 : correcto
            [0-9] { 1 } : Descripcion
              0 : Error general
              1 : Registro existente
              2 : campos no validos
              3 : Registro no existente
              4 : informacion no match
  ==================================================
  */
  {
    code: 'US0000',
    msg: 'Error al interactuar con Usurio'
  },
  {
    code: 'US0100',
    msg: 'Usuario registrado correctamente'
  },
  {
    code: 'US0001',
    msg: 'El email ya pertenece a otro usuario'
  },
  {
    code: 'US0002',
    msg: 'Error en la validacion del formulario'
  },
  // ===================================================
  {
    code: 'AU0000',
    msg: 'Error al interactuar con Auth'
  },
  {
    code: 'AU0003',
    msg: 'El usuario no existe'
  },
  {
    code: 'AU0004',
    msg: 'Contraseña incorrecta'
  },
  {
    code: 'AU0002',
    msg: 'Error en la validacion del formulario'
  },
  {
    code: 'AU0100',
    msg: 'Autencicacion correcta'
  },
  // ===================================================
  {
    code: 'MA0003',
    msg: 'Token no proporcionado'
  },
  {
    code: 'MA0000',
    msg: 'Token no valido'
  },
  // ===================================================
  {
    code: 'PR0000',
    msg: 'Error al interactuar con proyecto'
  },
  {
    code: 'PR0002',
    msg: 'Error en la validacion del formulario'
  },
  {
    code: 'PR0100',
    msg: 'Proyecto registrado correctamente'
  },
];

export function getMessage(code) {
  const err = messages.filter(error => error.code === code);
  if (err.length === 1) {
    return err[0];
  } else if (err.length > 1) {
    return err;
  } else {
    return getError('0000');
  }
}
export function report(code) {
  const data = getMessage(code);

  console.log(
    chalk.red('••• ') +
    chalk.white(`Error: `) +
    chalk.yellow(data.code+' ') +
    chalk.white(`Message: `) +
    chalk.yellow(data.msg+' ')
  );
}