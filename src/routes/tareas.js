import { Router } from "express";
import { check } from "express-validator";

import Auth from "../middlewares/auth.js";

import { addTarea, getTareas, updateTarea, deleteTarea } from "../controllers/tareas.js";

const router = Router();

router.post('/', Auth, [
  check('nombre', 'El nombre de la tarea es obligatorio').not().isEmpty(),
  check('proyecto', 'El id del proyecto es obligatorio').not().isEmpty()
], addTarea);

router.get('/', Auth, [
  check('proyecto', 'El id del proyecto es obligatorio').not().isEmpty()
], getTareas);

router.put('/tarea/:id', Auth, updateTarea);

router.delete('/tarea/:id', Auth, deleteTarea);



export default router;