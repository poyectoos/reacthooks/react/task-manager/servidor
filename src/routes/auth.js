import { Router } from "express";
import { check } from "express-validator";

import Auth from "../middlewares/auth.js";

import { logIn, authenticated } from "../controllers/auth.js";


const router = Router();

/*
==================================================
          La ruta base es /api/usuarios
==================================================
*/

/*
==================================================
          Crear un usuario
==================================================
*/
router.post('/', [
  check('email', 'Agrega un email valido').isEmail(),
  check('password', 'La contraseña debe tener como minimo 8 caracteres').isLength({ min: 8 })
], logIn);


router.get('/', Auth, authenticated);

export default router;