import { Router } from "express";
import { check } from "express-validator";

import { addUser } from "../controllers/usuarios.js";

const router = Router();

/*
==================================================
          La ruta base es /api/usuarios
==================================================
*/

/*
==================================================
          Crear un usuario
==================================================
*/
router.post('/', [
  check('nombre', 'El nombre de usuario es obligatorio').not().isEmpty(),
  check('email', 'Agrega un email valido').isEmail(),
  check('password', 'La contraseña debe tener como minimo 8 caracteres').isLength({ min: 8 })
], addUser);

export default router;