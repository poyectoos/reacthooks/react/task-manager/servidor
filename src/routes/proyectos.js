import { Router } from "express";
import { check } from "express-validator";

import Auth from "../middlewares/auth.js";

import { addProyecto, getProyectos, updateProyecto, deleteProyecto } from "../controllers/proyectos.js";

const router = Router();

// Agregamos un proyecto
router.post('/', Auth, [
  check('nombre', 'El nombre del proyecto es obligatorio').not().isEmpty()
], addProyecto);

router.get('/', Auth, getProyectos);

router.put('/proyecto/:id', Auth, [
  check('nombre', 'El nombre del proyecto es obligatorio').not().isEmpty()
], updateProyecto);

router.delete('/proyecto/:id', Auth, deleteProyecto);


export default router;