import { validationResult } from "express-validator";

import Proyecto from "../models/Proyecto.js";

import { report, getMessage } from "../data-codes.js";

export const addProyecto = async (request, response) => {

  // Revisamos is hay errores en los campos
  const errors = validationResult(request);
  if (!errors.isEmpty()) {
    return response.status(400).send(errors.array());
  }

  try {
    const { nombre } = request.body;

    const { id } = request.credentials;

    const proyecto = new Proyecto({
      nombre,
      autor: id
    });

    await proyecto.save();
    
    return response.status(200).send({
      id : proyecto._id,
      nombre: proyecto.nombre,
      create_at: proyecto.create_at,
    })

  } catch (error) {
    console.log(error);
    return response.status(500).send({
      msg: "Error al interactuar con portafolio"
    });
  }
}
export const getProyectos = async (request, response) => {
  try {
    const { id } = request.credentials;
    
    const proyectos = await Proyecto.find({ autor: id }).sort({ create_at: -1 });

    return response.status(200).send(proyectos);

  } catch (error) {
    console.log(error);
    return response.status(500).send({
      msg: "Error al interactuar con portafolio"
    });
  }
}
export const updateProyecto = async (request, response) => {
  // Revisamos is hay errores en los campos
  const errors = validationResult(request);
  if (!errors.isEmpty()) {
    return response.status(400).send(errors.array());
  }

  try {
    const { nombre } = request.body;
    const { id } = request.params;
    const proyecto = {}
    if (nombre) {
      proyecto.nombre = nombre;
    }

    const previo = await Proyecto.findById(id);
    // El proyecto no existe
    if (!previo) {
      return response.status(404).send({ msg: 'El proyecto no existe' });
    }
    // QUe solo el autor lo pueda editar
    if (previo.autor.toString() !== request.credentials.id) {
      return response.status(401).send({ msg: 'Usuario no autorizado' });
    }

    const answer = await Proyecto.findOneAndUpdate({ _id: id }, { $set: proyecto }, { new: true });

    return response.status(200).send({
      data: {
        id : answer._id,
        nombre: answer.nombre,
        create_at: answer.create_at,
      }
    });

  } catch (error) {
    console.log(error);
    return response.status(500).send({
      msg: "Error al interactuar con portafolio"
    });
  }
}
export const deleteProyecto = async (request, response) => {
  try {
    const { id } = request.params;

    const proyecto = await Proyecto.findById(id);
    // El proyecto no existe
    if (!proyecto) {
      return response.status(404).send({ msg: 'El proyecto no existe' });
    }
    // QUe solo el autor lo pueda editar
    if (proyecto.autor.toString() !== request.credentials.id) {
      return response.status(401).send({ msg: 'Usuario no autorizado' });
    }
    // Eliminacion
    await Proyecto.findOneAndRemove({ _id: id });
    
    return response.status(200).send({ msg: 'Proyecto eliminado correctamente' });
    
  } catch (error) {
    console.log(error);
    return response.status(500).send({
      msg: "Error al interactuar con portafolio"
    });
  }
}