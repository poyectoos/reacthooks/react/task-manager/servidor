import bcrypt from "bcryptjs";
import { validationResult } from "express-validator";
import jwt from "jsonwebtoken";

import Usuario from "../models/Usuario.js";

import { getMessage, report } from "../data-codes.js";
/*
==================================================
          Funcion para autenticar usuario
==================================================
*/
export const logIn = async (request, response) => {
  const errors = validationResult(request);
  if (!errors.isEmpty()) {
    response.status(400).send(errors.array());
  }

  const { email, password } = request.body;
  
  try {
    // Comprobamos si el usuario existe
    
    const existente = await Usuario.findOne({ email });
    
    if (!existente) {
      return response.status(404).send({ msg: 'Usuario o contraseña incorrecta' });
    }
    // Validamos su pssword
    const correcta = await bcrypt.compare(password, existente.password);
    if (!correcta) {
      return response.status(404).send({ msg: 'Contraseña o usuario incorrecta' });
    }
    // Si todo es correcto se registra ek webtoken
    // Se crea el payload
    const payload = {
      data: {
        id: existente._id,
        nombre: existente.nombre,
        email: existente.email,
        create_at: existente.create_at,
      }
    }
    // Se firma el token
    jwt.sign(payload, process.env.SECRET_WORD, {
      // expiresIn: 3600
    }, (error, token) => {
      if (error) {
        report('0002');
        throw error;
      }
      //delete respuesta.password;
      return response.status(200).send({ token });
    });



  } catch (error) {
    console.log(error);
    return response.status(500).send({ msg: 'Error en elservidor' });
  }

}
// Obtemenos al usuario autenticado
export const authenticated = async (request, response) => {

  try {
    const { id } = request.credentials;
    const usuario = await Usuario.findOne({ _id: id });

    return response.send({
      id: usuario._id,
      nombre: usuario.nombre,
      email: usuario.email,
      create_at: usuario.create_at
    })

  }catch (error) {
    console.log(error);
    return response.status(500).send({
      msg: "Error al interactuar con tareas"
    });
  }
}