import Usuario from "../models/Usuario.js";
import bcrypt from "bcryptjs";
import { validationResult } from "express-validator";
import jwt from "jsonwebtoken";

import { getMessage, report } from "../data-codes.js";
/*
==================================================
          Funcion para crear usuario
==================================================
*/
export const addUser = async (request, response) => {
  // Revisamos is hay errores en los campos
  const errors = validationResult(request);
  if (!errors.isEmpty()) {
    return response.status(400).send(errors.array());
  }
  try {
    const { nombre, password, email } = request.body;

    // Verificamos que el usuario se unico
    const previo = await Usuario.findOne({ email });

    if (previo) {
      return response.status(400).send({ msg: 'El usuario ya existe' });
    } else {

      // Se genera el salt
      const salt = await bcrypt.genSalt(10);

      // Guardar registros
      const usuario = new Usuario({
        nombre,
        password: await bcrypt.hash(password, salt),
        email
      });

      await usuario.save();

      // Se crea el payload
      const payload = {
        ... getMessage('US0100'),
        data: {
          id: usuario._id,
          nombre: usuario.nombre,
          email: usuario.email,
          create_at: usuario.create_at,
        }
      }
      // Se firma el token
      jwt.sign(payload, process.env.SECRET_WORD, {
        //expiresIn: 3600
      }, (error, token) => {
        if (error) {
          report('0002');
          throw error;
        }
        //delete respuesta.password;
        return response.status(200).send({ token });
      });
    }

  } catch (error) {
    console.log(error);
    return response.status(500).send({ msg: 'Error en el servidor' });
  }
}