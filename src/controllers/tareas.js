import { validationResult } from "express-validator";

import Proyecto from "../models/Proyecto.js";
import Tarea from "../models/Tarea.js";


export const addTarea = async (request, response) => {
  // Revisamos is hay errores en los campos
  const errors = validationResult(request);
  if (!errors.isEmpty()) {
    return response.status(400).send(errors.array());
  }

  try {
    const { proyecto, nombre } = request.body;
    const { id } = request.credentials;
    const proyectoExistente = await Proyecto.findOne({ _id: proyecto });

    if (!proyectoExistente) {
      return response.status(404).send({ msg: 'El proyecto no existe' });
    }
    if (proyectoExistente.autor.toString() !== id) {
      return response.status(401).send({ msg: 'Solo el autor puede agregar tareas al proyecto' });
    }

    const tarea = new Tarea({
      nombre,
      proyecto
    });

    await tarea.save();

    return response.send({
      id: tarea._id,
      nombre: tarea.nombre,
      estado: tarea.estado,
      proyecto: tarea.proyecto,
      create_at: tarea.create_at,
    });
  } catch (error) {
    console.log(error);
    return response.status(500).send({
      msg: "Error al interactuar con tareas"
    });
  }
}
export const getTareas = async (request, response) => {
  // Revisamos is hay errores en los campos
  const errors = validationResult(request);
  if (!errors.isEmpty()) {
    return response.status(400).send(errors.array());
  }
  try {
    const { proyecto } = request.query;
    const { id } = request.credentials;
    const proyectoExistente = await Proyecto.findOne({ _id: proyecto });

    if (!proyectoExistente) {
      return response.status(404).send({ msg: 'El proyecto no existe' });
    }
    if (proyectoExistente.autor.toString() !== id) {
      return response.status(401).send({ msg: 'Solo el autor puede obtener las tareas del proyecto' });
    }

    const tareas = await Tarea.find({ proyecto: proyecto }).sort({ create_at: -1 });

    return response.status(200).send(tareas);

  } catch (error) {
    console.log(error);
    return response.status(500).send({
      msg: "Error al interactuar con tareas"
    });
  }
}
export const updateTarea = async (request, response) => {
  try {
    const { nombre, estado } = request.body;
    const { id } = request.credentials;
    // Buscamos la tarea que se va a editar
    const tarea = await Tarea.findOne({ _id: request.params.id });
    // Comprobamos que exista
    if (!tarea) {
      return response.status(404).send({ msg: 'La tarea no existe' });
    }
    // Buscamos el proyecto al que pertenece la tarea
    const proyecto = await Proyecto.findOne({ _id: tarea.proyecto });
    // Comprobamos que exista
    if (!proyecto) {
      return response.status(404).send({ msg: 'El proyecto no existe' });
    }
    // Comprobamos que el man que va a editar la tarea sea el autor del proyecto
    if (proyecto.autor.toString() !== id) {
      return response.status(401).send({ msg: 'Solo el autor puede agregar tareas al proyecto' });
    }

    // Editamos la tarea
    const nuevaTarea = {}

    if (nombre) {
      nuevaTarea.nombre = nombre
    }
    if (estado !== null) {
      nuevaTarea.estado = estado
    }

    const answer = await Tarea.findOneAndUpdate({ _id: request.params.id }, nuevaTarea, { new: true });
    return response.status(200).send({
      id: answer._id,
      nombre: answer.nombre,
      estado: answer.estado,
      proyecto: answer.proyecto,
      create_at: answer.create_at,
    });
  } catch (error) {
    console.log(error);
    return response.status(500).send({
      msg: "Error al interactuar con tareas"
    });
  }
}
export const deleteTarea = async (request, response) => {
  try {
    const { id } = request.credentials;
    // Buscamos la tarea que se va a editar
    const tarea = await Tarea.findOne({ _id: request.params.id });
    // Comprobamos que exista
    if (!tarea) {
      return response.status(404).send({ msg: 'La tarea no existe' });
    }
    // Buscamos el proyecto al que pertenece la tarea
    const proyecto = await Proyecto.findOne({ _id: tarea.proyecto });
    // Comprobamos que exista
    if (!proyecto) {
      return response.status(404).send({ msg: 'El proyecto no existe' });
    }
    // Comprobamos que el man que va a editar la tarea sea el autor del proyecto
    if (proyecto.autor.toString() !== id) {
      return response.status(401).send({ msg: 'Solo el autor puede agregar tareas al proyecto' });
    }

    await Tarea.findOneAndDelete({ _id: request.params.id });

    //Eliminamos latarea
    return response.status(200).send({ msg: 'Tarea eliminada correctamente' })
  } catch (error) {
    console.log(error);
    return response.status(500).send({
      msg: "Error al interactuar con tareas"
    });
  }
}