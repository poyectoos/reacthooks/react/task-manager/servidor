import mongoose from "mongoose";
import dotenv from "dotenv";
import chalk from "chalk";

import { report } from "../data-codes.js";

dotenv.config({
  path: 'variables.env'
});


export const conexion = async () => {
  try {
    await mongoose.connect(process.env.DB_ENDPOINT, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true
    });
    console.log(
      chalk.cyan('••• ') +
      chalk.white('Conexion a Database Exitosa')
    );
  } catch (error) {
    report('0001');
    console.log(error);
    process.exit(1);
  }
}
