import express from "express";
import cors from "cors";
import morgan from "morgan";
import chalk from "chalk";

import { conexion } from "./database/config.js"

import UsuariosRouter from "./routes/usuarios.js";
import AuthRouter from "./routes/auth.js";
import ProyectosRouter from "./routes/proyectos.js";
import TareasRouter from "./routes/tareas.js";


const app = express();
// Obtenemos puerto
const port = process.env.PORT || 3312;
// Establecemos el puerto
app.set('port', port);
// Conectamos a la base de datos
conexion();

/*
==================================================
          middlewares
==================================================
*/
app.use(express.json({ extended: true }));
app.use(morgan('dev'));
app.use(cors());
/*
==================================================
          Se importan rutas
==================================================
*/
app.use('/api/usuarios', UsuariosRouter);
app.use('/api/auth', AuthRouter);
app.use('/api/proyectos', ProyectosRouter);
app.use('/api/tareas', TareasRouter);
app.use('/', (req, res) => {
  res.send("Hello there!!!");
});
/*
==================================================
          Arrancamos en el puerto
==================================================
*/

app.listen(app.get('port'), '0.0.0.0', () => {
  console.log(
    chalk.cyan('••• ') +
    chalk.white('Servidor en puerto: ') +
    chalk.yellow(app.get('port'))
  );
});