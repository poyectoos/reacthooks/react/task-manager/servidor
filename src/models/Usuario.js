import mongoose from "mongoose";

const Schema = mongoose.Schema({
  nombre: {
    type: String,
    required: true,
    trim: true
  },
  password: {
    type: String,
    required: true,
    trim: true,
  },
  email: {
    type: String,
    required: true,
    trim: true,
    unique: true
  },
  create_at: {
    type: Date,
    default: Date.now()
  }
});

export default mongoose.model("Usuario", Schema);