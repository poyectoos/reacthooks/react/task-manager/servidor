import mongoose from "mongoose";

const Schema = mongoose.Schema({
  nombre: {
    type: String,
    required: true,
    trim: true
  },
  estado: {
    type: Boolean,
    default: false
  },
  proyecto: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Proyecto'
  },
  create_at: {
    type: Date,
    default: Date.now()
  }
});


export default mongoose.model("Tarea", Schema);