import mongoose from "mongoose";

const Schema = mongoose.Schema({
  nombre: {
    type: String,
    required: true,
    trim: true
  },
  autor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Usuario'
  },
  create_at: {
    type: Date,
    default: Date.now()
  }
});


export default mongoose.model("Proyecto", Schema);