# Despliegue en heroku

1. Crear la aplicacion
La aplicacion puede ser creada desde el dashboard de heroku o mediante el CLI
2. Agregar el remote
`heroku git:remote -a "Nombre de la aplicacion"`
3. Se agregan las variables de entorno en el dashboard de heroku
4. Cargar los cambios a Heroku
`git push heroku master`
